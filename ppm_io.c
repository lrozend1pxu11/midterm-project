// Lucas Rozendaal | Peiyuan Xu
// lrozend1 pxu11
// ppm_io.c
// 601.220, Spring 2020

#include <assert.h>
#include "ppm_io.h"
#include <stdlib.h>
#include <string.h>

/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */

#define INTEGER_MAX 2147483647
Image * read_ppm(FILE *fp) {

  // check that fp is not NULL
  assert(fp);

  char file_type[3];
  fscanf(fp, " %s", file_type);
  if (strcmp(file_type, "P6") != 0) {
    return NULL;
  }
    
  int num_rows;
  int num_cols;
  int colors;

  char c;
   if (fscanf(fp, " %c", &c) != 1) {
    return NULL;
  }

  if (c == '#') {
    while( c!= '\n') 
      c = fgetc(fp);
  }else {
    ungetc(c, fp);
  }
  int num = fscanf(fp, " %d %d %d ", &num_cols, &num_rows, &colors);

  if (num != 3) {
      return NULL;
  }

  if (num_cols <= 0 || num_rows <= 0) {
    return NULL;
  }
  
  if (colors != 255) {
    return NULL;
  }
  Image *im = malloc(sizeof(Image));
  im->rows = num_rows;
  im->cols = num_cols;
  Pixel *pix = malloc(sizeof(Pixel) * im->rows * im->cols);
  im->data = pix;
  fread(im->data, sizeof(Pixel), im->rows * im->cols, fp);
  fclose(fp);
  return im;

}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    //fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
    return 7;
  }
  fclose(fp);
  return num_pixels_written;
}


/* input a PPM-formatted image
 * return a copy of that image
 */
Image *copy(Image* original) {
  Image * result = malloc(sizeof(Image));
  result->rows = original->rows;
  result->cols = original->cols;
  memcpy(result->data, original->data, original->rows * original->cols);
  return result;
}



/*void delete(Image* original) {
  for (int row = 0; row < original->rows; row++) {
    for (int col = 0; col < original->cols; col++) {
      // (original->data)[row * original->cols + col] ;
    }
  }
}*/
  
