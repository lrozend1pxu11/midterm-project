
//Lucas Rozendaal | Peiyuan Xu
#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include "ppm_io.h"

//int average(int a, int b, int c, int d);

/* Takes the command line arguments, reads input file,  runs the appropriate image manipulation functions, checks for errors, and writes the modified image to an output file
 */
int convert(int argc, char* argv[]);

/* Changes the exposure of the original image by multiplying each pixel's
color channel values by 2 ^ ev
 */
void  exposure(Image *original, double  ev);

/* 
Blends two input images into one using a given blend ratio alpha. The value of each pixel in the output blended image is a linear combination of the corresponding pixel values in the input images. 
 */
Image* a_blend(Image *input1, Image *input2, double  alpha);

/* Creates a zoomed in image with twice as many rows and columns as the original image. Each pixel from the original image is duplicated into a 2x2 square of pixels. Outputs the new zoomed in image. 
 */
Image* zoomIn(Image *original);

/* Creates a zoomed out image with half as many columns and rows as the original image. Each pixel in the new image is made from a 2x2 square of pixels in the input image with the averages of each of the three color channels of the four pixels as its color channels. Outputs the zoomed out image. 
 */
Image* zoomOut(Image *original);

/* Applies a pointilism-like effect to an input image by randomly selecting 3% of pixels in the input image and creating small filled circles centered on those pixels. 
 */
void pointilism(Image *original);

/* Creates a swirled image from an input image by performing a non-linear deformation on the image based on a center and distortion scale. Returns the swirled image. 
 */
Image* swirl(Image *original, int col, int row, int distortion );

/* Blurs the original image by creating a gaussian filter matrix from a sigma "blur_val" and then convolving it with the original image. 
 */
Image* blur(Image *original, double sigma);
#endif
