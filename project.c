//Lucas Rozendaal | Peiyuan Xu
#include "imageManip.h"
#include "ppm_io.h"

int main(int argc, char *argv[]) {
 int result =  convert(argc, argv);
 if (result == 1) {
   return 1;
 } else if (result == 2) {
   return 2;
 } else if (result == 3) {
   return 3;
 } else if (result == 4) {
   return 4;
 } else if (result == 5) {
   return 5;
 } else if (result == 6) {
   return 6;
 } else if (result == 7) {
   return 7;
 } else {
   return 0;
 }
}
