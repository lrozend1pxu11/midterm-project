// Lucas Rozendaal | Peiyuan Xu
// lrozend1 | pxu11
// imageManip.c
// 601.220, Spring 2020

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "ppm_io.h"
#include "imageManip.h"

#define PI 3.14159265358979323846

//declaring smaller subfunctions for blur

double sq(double a);

double *create_g_matrix(double sigma);

void convolve(Image *original, Image *result,  double *g_matrix, double sigma, int row, int col);


int convert(int argc, char** argv) {
  //copies elements of argv into individual strings
  char arg1[64];
  char arg2[64];
  char arg3[64];
  char arg4[64];
  char arg5[64];
  char arg6[64];
  strcpy(arg1, argv[1]);
  if(argc > 2) {
    strcpy(arg2, argv[2]);
    if(argc > 3) {
      strcpy(arg3, argv[3]);
      if(argc > 4) {
	strcpy(arg4, argv[4]);
	if(argc > 5) {
	  strcpy(arg5, argv[5]);
	  if(argc > 6) {
	    strcpy(arg6, argv[6]);
	  }
	}
      }
    }
  }
  
  // Checks that input and output filename were supplied
  if (argc < 3) {
    fprintf(stderr, "did not supply both input and output filenames");
    return 1;
  }

  // Check that operation was specified
  if (argc < 4) {
    fprintf(stderr, "No operation specified");
    return 4;
  }

  //Carries out the exposure operation
  if(strcmp(argv[3], "exposure") == 0) {
    if(argc != 5) {
      fprintf(stderr, "Incorrect number of arguments");
      return 5;
    }

    double exp;
    if (sscanf(argv[4], "%lf", &exp) != 1) {
      fprintf(stderr, "Argument not floating point");
      return 6;
    }

    if (exp < -3 || exp > 3) {
      fprintf(stderr, "arguments out of range");
      return 6;
    }

    // Opens the input file
    FILE* r_file  = fopen(argv[1], "rb");
    if (r_file == NULL) {
      fprintf(stderr, "Input file cannot be opened");
      return 2;
    }
    //creates an image from the info in the input file
    Image *original = read_ppm(r_file);
    if (original == NULL) {
      fprintf(stderr, "Input file not valid");
      return 3;
    }
    // runs the exposure function
    exposure(original, exp);
    //outputs to the output file
    FILE* w_file = fopen(argv[2], "wb");
    if (w_file == NULL) { 
      fprintf(stderr, "cannot open file for writing");
      return 7;
    }
    if (write_ppm(w_file, original) == 7) {
      fprintf(stderr, "writing output somehow failed");
      return 7;
    }
    //frees the image data and then the image itself
    free(original->data);
    free(original);
    return 0;
  }

  //Carries out the blend operation
  if(strcmp(argv[3], "blend") == 0) {
    if(argc != 6) {
      fprintf(stderr, "Incorrect number of arguments");
      return 5;
    }

    //opens the two input files
    FILE* r_file1  = fopen(argv[1], "rb");
    FILE* r_file2 = fopen(argv[4], "rb");
    if (r_file1 == NULL || r_file2 == NULL) {
      fprintf(stderr, "Input file cannot be opened");
      return 2;
    }
    Image *original1 = read_ppm(r_file1);
    Image *original2 = read_ppm(r_file2);
    if (original1== NULL || original2== NULL) {
      fprintf(stderr, "Input file not valid");
      return 3;
    }
    double alpha;
    if (sscanf(argv[5], "%lf", &alpha) != 1) {
      fprintf(stderr, "Argument not floating point");
      return 6;
    }
    if (alpha < 0 || alpha  > 1) {
      fprintf(stderr, "Arguments out of range");
      return 6;
    }
    //creates the blended image
    Image *blended_image = a_blend(original1, original2, alpha);
    if (blended_image == NULL) {
      fprintf(stderr, "fail to allocate image");
      return 8;
    }
    //outputs to the output file
    FILE* w_file  = fopen(argv[2], "wb");
    if (w_file == NULL) {
      fprintf(stderr, "cannot open file for writing");
      return 7;
    }
    if ( write_ppm(w_file, blended_image) == 7) {
      fprintf(stderr, "writing output somehow failed");
      return 7;
    }
    // frees image data and images
    free(original1->data);
    free(original1);
    free(original2->data);
    free(original2);
    free(blended_image->data);
    free(blended_image);
    return 0;
  }

  // carries out the zoom in operation
  if(strcmp(argv[3], "zoom_in") == 0) {
    if(argc != 4) {
      fprintf(stderr, "Incorrect number of arguments");
      return 5;
    }
    // opens input file
    FILE* r_file  = fopen(argv[1], "rb");
    if (r_file == NULL) {
      fprintf(stderr, "Input file cannot be opened");
      return 2;
    }
    // creates image from input file data
    Image *original = read_ppm(r_file);
    if (original == NULL) {
      fprintf(stderr, "Input file not valid");
      return 3;
    }
    //creates zoomed in image
    Image *zoomed_in_image = zoomIn(original);
    if (zoomed_in_image == NULL) {
      fprintf(stderr, "failed to properly allocate image");
      return 8;
    }
    //writes the zoomed in image to the output file
    FILE* w_file  = fopen(argv[2], "wb");
    if (w_file == NULL) {
      fprintf(stderr, "cannot open file for writing");
      return 7;
    }
    if (write_ppm(w_file, zoomed_in_image) == 7) {
      fprintf(stderr, "writing output somehow failed");
      return 7;
    }
    // frees image data and images
    free(original->data);
    free(original);
    free(zoomed_in_image->data);
    free(zoomed_in_image);
    return 0;
  }

  //carries out the zoom out operation
  if(strcmp(argv[3], "zoom_out") == 0) {
    if(argc != 4) {
      fprintf(stderr, "incorrect number of arguments");
      return 5;
    }
    //opens the inpiut file
    FILE* r_file  = fopen(argv[1], "rb");
    if (r_file == NULL) {
      fprintf(stderr, "Input file cannot be opened");
      return 2;
    }
    //creates an image from the input file data
    Image *original = read_ppm(r_file);
    if (original == NULL) {
      fprintf(stderr, "Input file not valid");
      return 3;
    }
    //creates a zoomed out image
    Image *zoomed_out_image = zoomOut(original);
    if (zoomed_out_image == NULL) {
      fprintf(stderr, "failed to properly allocate image");
      return 8;
    }
    //writes the zoomed out image to the output file
    FILE* w_file  = fopen(argv[2], "wb");
    if (w_file == NULL) {
      fprintf(stderr, "cannot open file for writing");
      return 7;
    }
    if (write_ppm(w_file, zoomed_out_image) == 7) {
      fprintf(stderr, "writing output somehow failed");
      return 7;
    }
    //frees the image data and the images
    free(original->data);
    free(original);
    free(zoomed_out_image->data);
    free(zoomed_out_image);
    return 0;
  }
  //carries out the pointilism operation
  if(strcmp(argv[3], "pointilism") == 0) {
    if(argc != 4) {
      fprintf(stderr, "incorrect number of arguments");
      return 5;

    }
    //opens the input file
    FILE* r_file  = fopen(argv[1], "rb");
    if (!r_file) {
      fprintf(stderr, "Input file cannot be opened");
      return 2;
    }
    //creates an image from the input file data
    Image *original = read_ppm(r_file);
    //executes the pointilism function, modifying the original image
    pointilism(original);
    //writes the modified image to an output file
    FILE* w_file  = fopen(argv[2], "wb");
    if (w_file == NULL) {
      fprintf(stderr, "cannot open file for writing");
      return 7;
    }
    if (write_ppm(w_file, original) == 7) {
      fprintf(stderr, "writing output somehow failed");
      return 7;
    }  
    free(original->data);
    free(original);
    return 0;
    }

  //carries out the swirl operation
  if(strcmp(argv[3], "swirl") == 0) {
    if(argc != 7) {
      fprintf(stderr, "incorrect number of arguments");
      return 5;
    }
    //opens the input file
    FILE* r_file  = fopen(argv[1], "rb");
    if (r_file == NULL) {
      fprintf(stderr, "Input file cannot be opened");
      return 2;
    }
    //creates an image from the input file data
    Image *original = read_ppm(r_file);
    if (original == NULL) {
      fprintf(stderr, "Input file not valid");
      return 3;
    }
    int cx, cy, s;
    if (sscanf(argv[4], " %d", &cx) != 1) {
      fprintf(stderr, "cx not integer");
      return 6;
    }

    if (sscanf(argv[5], " %d", &cy) != 1) {
      fprintf(stderr, "cy not integer");
      return 6;
    }

    if (sscanf(argv[6], " %d", &s) != 1) {
      fprintf(stderr, "s not integer");
      return 6;
    }

    if (cx < 0 || cx >= original->cols || cy < 0 || cy >= original->rows || s <= 0) {
      fprintf(stderr, "arguments out of range");
      return 6;
    }
    //creates a swirled image using the swirl function
    Image *swirled_image = swirl(original, cx, cy, s);
    if (swirled_image == NULL) {
      fprintf(stderr, "failed to properly allocate image");
      return 8;
    }

    //writes the swirled image to an output file
    FILE* w_file  = fopen(argv[2], "wb");
    if (w_file == NULL) {
      fprintf(stderr, "cannot open file for writing");
      return 7;
    }
    if (write_ppm(w_file, swirled_image) == 7) {
      fprintf(stderr, "writing output somehow failed");
      return 7;
    }
    //frees the image data and the images
    free(original->data);
    free(original);
    free(swirled_image->data);
    free(swirled_image);
    return 0;
  }
  if(strcmp(argv[3], "blur") == 0) {
    if(argc != 5) {
      fprintf(stderr, "incorrect number of arguments");
      return 5;
    }
    FILE* r_file  = fopen(argv[1], "rb");
    if (!r_file) {
      fprintf(stderr, "Input file cannot be opened");
      return 2;
    }
    //creates an image from the input file data
    Image *original = read_ppm(r_file);
    if (original == NULL) {
       fprintf(stderr, "Input file not properly formatted");
       return 3;
    }

    double sigma;
    if (sscanf(argv[4], "%lf", &sigma) != 1) {
      fprintf(stderr, "Argument not floating point");
      return 6;
    }

    if (sigma < 0 ) {
      fprintf(stderr, "Argument out of range");
      return 6;
    }

    //executes the blur function, creating a new blurred image
    Image *result = blur(original, sigma);
    //writes the modified image to an output file
    FILE* w_file  = fopen(argv[2], "wb");
    if (w_file == NULL) {
        fprintf(stderr, "cannot open file for writing");
	return 7;
    }
    if (write_ppm(w_file, result) == 7) {
        fprintf(stderr, "writing output somehow failed");
        return 7;
    }
    free(original->data);
    free(original);
    free(result->data);
    free(result);
    return 0;
  }
  else {

    fprintf(stderr, "operation not valid");
    return 4;
  }

  fprintf(stderr, "No errors detected");
  return 0; 
}

//carries out the exposure operation on the original image
void exposure(Image *original, double ev) {
  int totalRows = original -> rows;
  int totalCols = original -> cols;
  //iterates through every pixel in the image
  for(int row = 0; row < totalRows; row++) {
    for(int col = 0; col < totalCols; col++) {
      int input_r = original->data[row * totalCols + col].r;
      //calculates new color value for r
      input_r *= pow(2,ev);
      //check that new value does not exceed max (255)
      if(input_r> 255) {
	input_r= 255;
      }
      //sets new color value
      original->data[row * totalCols + col].r = (unsigned char) input_r;

      //same as for r
      int input_b = original->data[row * totalCols + col].b;
      input_b *= pow(2,ev);
      if (input_b > 255) {
	input_b= 255;
      }
      original->data[row * totalCols + col].b = (unsigned char) input_b;

      //same as for r
      int input_g = original->data[row* totalCols + col].g;
      input_g *= pow(2,ev);
      if (input_g > 255) {
	input_g= 255;
      }
      original->data[row * totalCols + col].g = (unsigned char) input_g;
    }
  }
}

//carries out the blend operation, returning a new blended image
Image *a_blend(Image *input1, Image *input2, double  alpha) {
  //allocates space for the result
  Image * result = malloc(sizeof(Image));
  if (result == NULL) {
    return NULL;
  }
  int minRow = 0;
  int maxRow = 0;
  //calculates the dimensions of the new image (rows/cols)
  if(input2->rows < input1->rows) {
    minRow = input2->rows;
    maxRow = input1->rows;
  } else {
    minRow = input1->rows;
    maxRow = input2->rows;
  }

  int minCol = 0;
  int maxCol = 0;
  if(input2->cols < input1->cols) {
    minCol = input2->cols;
    maxCol = input1->cols;
  } else {
    minCol = input1->cols;
    maxCol = input2->cols;
  }

  result->rows = maxRow;
  result->cols = maxCol;

  Pixel * pix = malloc(sizeof(Pixel) * result->rows * result->cols);
  if (pix == NULL) {
    return NULL;
  }
  result->data = pix;
  //iterates through every pixel of the new image, sets the new values
  for(int row = 0; row < minRow; row++) {
    for(int col = 0; col < minCol; col++) {
      result->data[row * result->cols + col].g = (unsigned char)  (alpha * (input1->data)[row * input1->cols + col].g + (1-alpha) * (input2->data)[row * input2->cols + col].g);
      result->data[row * result->cols  + col].b = (unsigned char) (alpha * (input1->data)[row * input1->cols  + col].b + (1-alpha) * (input2->data)[row * input2->cols + col].b);
      result->data[row * result->cols  + col].r = (unsigned char) (alpha * (input1->data)[row * input1->cols  + col].r + (1-alpha) * (input2->data)[row * input2->cols + col].r);
    }
  }

  for (int row = 0; row < minRow; row++) {
    for (int col = minCol; col < maxCol; col++) {
      if (minCol == input1->cols) {
	result->data[row * result->cols + col].g = input2->data[row * input2->cols + col].g;
	result->data[row * result->cols + col].b = input2->data[row * input2->cols + col].b;
	result->data[row * result->cols + col].r = input2->data[row * input2->cols + col].r;
      }

      if (minCol == input2->cols) {
	result->data[row * result->cols + col].g = input1->data[row * input1->cols + col].g;
	result->data[row * result->cols + col].b = input1->data[row * input1->cols + col].b;
	result->data[row * result->cols + col].r = input1->data[row * input1->cols + col].r;
      }
    }
  }

  for (int row = minRow; row < maxRow; row++) {
    for (int col = 0; col < minCol; col++) {
      if (minRow == input1->rows) {
	result->data[row * result->cols + col].g = input2->data[row * input2->cols + col].g;
	result->data[row * result->cols + col].b = input2->data[row * input2->cols + col].b;
	result->data[row * result->cols + col].r = input2->data[row * input2->cols + col].r;
      }

      if (minRow == input2->rows) {
	result->data[row * result->cols + col].g = input1->data[row * input1->cols + col].g;
	result->data[row * result->cols + col].b = input1->data[row * input1->cols + col].b;
	result->data[row * result->cols + col].r = input1->data[row * input1->cols + col].r;
      }
    }
  }

  for(int row = minRow; row < maxRow; row++) {
    for(int col = minCol; col < maxCol; col++) {
      result->data[row * result->cols  + col].g = 0;
      result->data[row * result-> cols + col].b = 0;
      result->data[row * result-> cols  + col].r = 0;
    }
  }
  return result;

  }

//carries out the zoom in operation, returning a new zoomed in image
Image *zoomIn(Image *original) {
  //allocates space for the result image
  Image * result =  malloc(sizeof(Image));
  if (result == NULL) {
    return NULL;
  }
  result->rows = 2 * original->rows;
  result->cols = 2 * original->cols;
  
  Pixel * pix = calloc(result->rows * result->cols,sizeof(Pixel));
  if (pix == NULL) {
    return NULL;
  }
  result->data=pix;

  int origrow = 0;
  int origcol = 0;
  //loops through the zoomed in image
  for (int row = 0; row < result->rows; row+=2) {
    origcol = 0;
    for (int col = 0; col < result ->cols; col+=2) {
      //sets color values
        result->data[row*result -> cols + col].r = original->data[origrow * original->cols + origcol].r;
        result->data[row*result -> cols + col + 1].r = original->data[origrow * original->cols + origcol].r;
        result->data[(row+1) * result-> cols + col].r = original->data[origrow * original->cols + origcol].r;
        result->data[(row+1) * result->cols + col + 1].r = original->data[origrow*original->cols + origcol].r;

        result->data[row*result->cols + col].g = original->data[origrow * original->cols + origcol].g;
	result->data[row*result -> cols + col + 1].g = original->data[origrow * original->cols + origcol].g;
	result->data[(row+1) * result-> cols + col].g = original->data[origrow * original->cols + origcol].g;
	result->data[(row+1) * result->cols + col + 1].g = original->data[origrow*original->cols + origcol].g;

	result->data[row*result->cols + col].b =result->data[row*result -> cols + col + 1].b = result->data[(row+1) * result-> cols + col].b = result->data[(row+1) * result->cols + col + 1].b = original->data[origrow*original->cols + origcol].b;
      origcol += 1;
    }
    origrow += 1;
  }
  return result;

}

//carries out the zoom out operation, returning a new zoomed out image
Image *zoomOut(Image *original) {
  //allocates space for the copy image
  Image *result = malloc(sizeof(Image));
  if (result == NULL) {
    return NULL;
  }
  result->rows = original->rows / 2;
  result->cols = original->cols / 2;
  Pixel *pixel = malloc(sizeof(Pixel) * result->rows * result->cols);
  if (pixel == NULL) {
    return NULL;
  }
  result->data=pixel;

  int resultRow = 0;
  int resultCol = 0;

  //loops through the zoomed out image
  for (int row = 0; row < original->rows; row+= 2) {
    resultCol = 0;
    for (int col = 0; col < original->cols; col+=2) {
      //determines and sets color values
      result->data[resultRow*result->cols + resultCol].r = (original->data[row * original->cols + col].r + original->data[row * original->cols + col + 1].r +original->data[(row + 1) * original->cols + col].r + original->data[(row + 1) * original->cols + col + 1].r) / 4;
      result->data[resultRow*result->cols + resultCol].g = (original->data[row * original->cols + col].g + original->data[row * original->cols + col + 1].g+ original->data[(row + 1) * original->cols + col].g+ original->data[(row + 1) * original->cols + col + 1].g) / 4;
      result->data[resultRow*result->cols + resultCol].b = (original->data[row * original->cols + col].b + original->data[row * original->cols + col + 1].b+ original->data[(row + 1) * original->cols + col].b+ original->data[(row + 1) * original->cols + col + 1].b) / 4;
      resultCol++;
    }
    resultRow++;
  }
  return result;
}

//Carries out the blur operation, 
Image *blur(Image *original, double sigma) {
  //allocates space for the filter, the output image, and the output image data
  double *g_matrix = create_g_matrix(sigma);
  Image *result = malloc(sizeof(Image));
  if (result == NULL) {
    return NULL;
  }
  result->rows = original->rows;
  result->cols = original->cols;
  Pixel *pix = malloc(sizeof(Pixel) * result->rows * result->cols);
  if (pix == NULL) {
    return NULL;
  }
  result->data = pix;

  //loops through the result image and convolves each pixel
  for(int row = 0; row < result->rows; row++) {
    for(int col = 0; col < result->cols; col++) {
      convolve(original, result, g_matrix, sigma, row, col);
    }
  }
  //frees the filter
  free(g_matrix);
  return result;
}


//carries out the pointilism operation on the original image
void pointilism(Image* original) {
  srand(1);
  //calculates the number of random pixels needed for the effect
  int num_pixels = (int)(original->rows * original->cols * 0.03);
  //loop through and generate a circle for each random pixel
  for(int i = 0; i < num_pixels; i++) {
    //generates the col + row of the circle's midpoint and the radius
    int pix_col = (rand() % (original->cols));
    int pix_row = (rand() % (original->rows));
    double radius = (rand() % 5) + 1.0;
    int totalRows = original -> rows;
    int totalCols = original -> cols;
    //loop the rows/cols around the circle to create it
    for(int row = pix_row - radius; row <= pix_row + radius; row++) {
      for(int col = pix_col - radius; col <= pix_col + radius; col++) {
	//boundary condition
	if((row >= 0) && (row < totalRows) && (col >= 0) && (col < totalCols)) {
	  //check if the point is within the circle, if yes, set its color values to be equal to the values of the center point
	  if((pow((row-pix_row), 2) + pow((col-pix_col), 2)) <= pow(radius, 2)) {
	    original->data[row * totalCols + col].r = original->data[pix_row * totalCols + pix_col].r;
            original->data[row * totalCols + col].b = original->data[pix_row * totalCols + pix_col].b;
	    original->data[row * totalCols + col].g = original->data[pix_row * totalCols + pix_col].g; 
	  }
	}
      }
    }
  }
}

//carries out the swirl operation and returns a swirled copy
Image *swirl(Image* original, int cx, int cy, int distortion){
  //allocates space for a copy
  Image *result = malloc(sizeof(Image));
  if (result == NULL) {
    return NULL;
  }
  result->rows = original->rows;
  result->cols = original->cols;
  Pixel *pix = malloc(sizeof(Pixel) * result->rows * result->cols);
  if (pix == NULL) {
    return NULL;
  }
  result->data = pix;
  //loops through the result image
  for (int row = 0; row < result->rows; row++) {
    for (int col = 0; col < result->cols; col++) {
      //calculates swirl effect to determine what the value of the pixel should be
      double  alpha = sqrt(pow(col-cx, 2) + pow(row - cy,2)) / distortion;
      int result_y = (int) ((col - cx) * sin(alpha) + (row - cy) * cos(alpha) + cy);
      int result_x = (int) ((col - cx) * cos(alpha) - (row - cy) * sin(alpha) + cx);
      //checks boundaries
      if (result_y < 0 || result_x < 0 || result_y >= original->rows || result_x >= original->cols) {
	result->data[row * result->cols + col].r = 0;
	result->data[row * result->cols + col].g = 0;
	result->data[row * result->cols + col].b = 0;
      }
      //sets color values in the result image
      else {
      result->data[row * result->cols + col].r = original->data[result_y * original->cols + result_x].r;
      result->data[row * result->cols + col].g = original->data[result_y * original->cols + result_x].g;
      result->data[row * result->cols + col].b = original->data[result_y * original->cols + result_x].b;
      }
    }
  }
  return result;
}

//outputs the square of whatever double is inputted
double sq(double a) {
  return a * a;
}

//Returns a gaussian filter created using an input sigma
double *create_g_matrix(double sigma) {
  //Determine num rows + cols for the matrix
  int g_rows = (10 * sigma);
  g_rows += (g_rows % 2 == 0);
  int g_cols = (10 * sigma);
  g_cols += (g_cols % 2 == 0);

  //allocate space for the matrix
  double *g_matrix = malloc(g_rows * g_cols * sizeof(double));
  //loop through and assign each element of the array the appropriate value using the formula
  for(int g_row = 0; g_row < g_rows; g_row++) {
    for(int g_col = 0; g_col < g_cols; g_col++) {
      int dx = abs((g_cols / 2) - g_col);
      int dy = abs((g_rows / 2) - g_row);
      g_matrix[(g_row*g_cols) + g_col]  = (1.0 / (2.0 * PI  * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
    }
  }
  return g_matrix;
}

//calculates what the color values of an input pixel should be by convolving its location on the original image with the filter
void convolve(Image *original, Image *result, double *g_matrix, double sigma, int row, int col) {
  int totalRows = original -> rows;
  int totalCols = original -> cols;
  int g_rows = (10 * sigma);
  g_rows += (g_rows % 2 == 0);
  int g_cols = (10 * sigma);
  g_cols += (g_cols % 2 == 0);
  double filter_sum = 0;
  double weighted_pixel_sum_r = 0;
  double weighted_pixel_sum_b = 0;
  double weighted_pixel_sum_g = 0;
  int go_to_first_row = g_rows / 2;
  int go_to_first_col = g_cols / 2;

  //loops through the filter
  for(int g_row = 0; g_row < g_rows; g_row++) {
    for(int g_col = 0; g_col < g_cols; g_col++) {
      //boundary condition
      if((row - go_to_first_row + g_row  >= 0) && (row - go_to_first_row + g_row < totalRows)) {
	if((col - go_to_first_col + g_col  >= 0) && (col - go_to_first_col + g_col < totalCols)) {
	  //adds up the weights of every element in the filter that you are counting (Should be 1 except for boundary cases)
	  filter_sum += g_matrix[g_row * g_cols + g_col];

	  //Do a weighted sum to figure out what the pixel values should be
	  weighted_pixel_sum_r += original->data[(row * totalCols + col) - (go_to_first_row * totalCols + go_to_first_col) + (g_row * totalCols + g_col)].r * g_matrix[g_row * g_cols + g_col];
	  weighted_pixel_sum_b += original->data[(row * totalCols + col) - (go_to_first_row * totalCols + go_to_first_col) + (g_row * totalCols + g_col)].b * g_matrix[g_row * g_cols + g_col];
	  weighted_pixel_sum_g += original->data[(row * totalCols + col) - (go_to_first_row * totalCols + go_to_first_col) + (g_row * totalCols + g_col)].g * g_matrix[g_row * g_cols + g_col];
	}
      }
    }
  }
  //set the pixel in the output image's color values to be what you calculated
  result->data[row * totalCols + col].r = weighted_pixel_sum_r / filter_sum;
  result->data[row * totalCols + col].g = weighted_pixel_sum_g / filter_sum;
  result->data[row * totalCols + col].b = weighted_pixel_sum_b / filter_sum;
}
