commit 367f98f2c013d730934e2863eaadfdf949e9ccd1
Merge: 199e6e0 d376055
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Fri Mar 27 11:26:27 2020 -0400

    finished alpha blend

commit 199e6e0547574f94a0abaf647601b89ef15edd2e
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Thu Mar 26 16:06:55 2020 -0400

    ignoring the comment finally succeeded

commit d376055908eb2540e9a3d4d3167482ff7be1ea34
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Thu Mar 26 14:19:27 2020 -0400

    added frees

commit 9f3087c64049d0eb21b66c471057a562b036b37f
Merge: cd293b2 d2b2afb
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Wed Mar 25 12:01:09 2020 -0400

    zooms working and incorporated more error conditions, files seem to be read properly

commit cd293b24e865a9590d9a3ea4d396783b5e830893
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Tue Mar 24 11:23:00 2020 -0400

    incorporated more error conditions

commit d2b2afbaca489180c9fcb7d99d922ff2da972023
Merge: 41a340d df71e4a
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Sat Mar 21 18:05:22 2020 -0400

    blur

commit 41a340dacbc945a4a8c24a949f448ae34905e902
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Sat Mar 21 17:59:28 2020 -0400

    blur

commit df71e4a63d20423607daa9cbc9b101ef812c6ca5
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Sat Mar 21 15:12:39 2020 -0400

    finished with swirl

commit 3dd1d7afe55fe803a0fcdac9c2753f61dcfed088
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Sat Mar 21 11:17:32 2020 -0400

    Makefile working properly

commit 023ebb225f5b6ba8c1de3b2f74c1ec61be9ca84c
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Sat Mar 21 10:58:07 2020 -0400

    finished exposure

commit 6933127913608731fa502dcc9453dd4644701efc
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Wed Mar 18 17:20:29 2020 -0400

    working better

commit 6e8098cd3e8ecad67b3aedbdc10a315e7cad534a
Merge: 2b9d0c6 838d699
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Wed Mar 18 15:15:14 2020 -0400

    Merge branch 'master' of https://bitbucket.org/lrozend1pxu11/midterm-project

commit 838d699cf68447840759df122e8cece4cacb0ab1
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Wed Mar 18 15:10:18 2020 -0400

    drafted pointilism

commit 2b9d0c67c74360ab2b1d3b8d10e5ef18328a4f66
Merge: ad033bd 46c0d34
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Tue Mar 17 15:34:03 2020 -0400

    working better

commit ad033bd66eefadaa264f8d22201c1831e6544b33
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Tue Mar 17 11:52:32 2020 -0400

    updating

commit 46c0d344aa9fe566faeee68a9f3be3f606b0ba46
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Tue Mar 17 11:36:33 2020 -0400

    merged!

commit 91766ca3e7ac6c6344c2e2234a51e25b1f642d89
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Tue Mar 17 11:33:40 2020 -0400

    still merging

commit 3fdba3f57eee9979f06e0131c53520d51836f584
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Tue Mar 17 11:27:14 2020 -0400

    putting our work together

commit 72de32c4166783d72d5b88ebd1ae1217191b4906
Merge: d38e886 40efea7
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Mon Mar 16 16:37:28 2020 -0400

    Merge branch 'master' of https://bitbucket.org/lrozend1pxu11/midterm-project

commit d38e88601dbe23fb30527b6c68e311ba0a033f74
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Mon Mar 16 16:37:23 2020 -0400

    better now

commit 07dca20eff300c766056ab664446282c2bf891aa
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Mon Mar 16 14:37:32 2020 -0400

    making progress

commit 40efea713ffebf397325442d0ad038b6659483e5
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Mon Mar 16 14:09:01 2020 -0400

    made a Makefile

commit 96828a2efb6a9c58836b15ef5ed21103238b2102
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Mon Mar 16 13:59:54 2020 -0400

    created main and a conversion function

commit bfad405170c1e00403630fc9bfea6d4a82261935
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Thu Mar 12 22:30:21 2020 -0400

    still working

commit a8c204f7d872c8ec53dba8405f44d40a065e1a84
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Thu Mar 12 16:41:54 2020 -0400

    updated

commit 95bd79fcf196d7236006622ab7231f255622c36f
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Thu Mar 12 12:27:10 2020 -0400

    main method improving

commit 36aecad3be684e9017a72cc37c5a376b89c3d688
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Thu Mar 12 12:23:27 2020 -0400

    making progress

commit 6d5f461d3145394e0abd11b5e555a21c644a1e2e
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Tue Mar 10 18:53:57 2020 -0400

    worked on exposure

commit 3390ba9a7b10d40153e05da6a98521caa9d0109d
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Tue Mar 10 18:33:13 2020 -0400

    modified exposure

commit fd9d46de2ee3571199f95eb452d86fa8372ac022
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Tue Mar 10 18:31:42 2020 -0400

    added Makefile successfully

commit ac7f37208d414ea71f5538352d9949f0398db17d
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Tue Mar 10 18:24:42 2020 -0400

    started project.c

commit 063c4121f8ef4e33fd8de37fc76156e4b3ba9ccc
Merge: 1a2c721 8524fe7
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Tue Mar 10 18:21:27 2020 -0400

    Merge branch 'master' of https://bitbucket.org/lrozend1pxu11/midterm-project

commit 1a2c7216a78ec1283746471cd4ec592eecba6a21
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Tue Mar 10 18:21:13 2020 -0400

    added imageManip.c

commit 8524fe75658263282632cc65a0818dfb658dbb5a
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Tue Mar 10 18:19:35 2020 -0400

    wrote ppm_io.h

commit cd50d792e307842fb8771c1b05e1c177ced0e97e
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Tue Mar 10 18:17:29 2020 -0400

    wrote imageManip.h

commit c0b0a8f8557877f0b9ac6012cd6f72590ed80b12
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Tue Mar 10 18:04:06 2020 -0400

    added a copy

commit d9cf25e7e044c365727410b1ba9a098f0434c540
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Tue Mar 10 17:35:44 2020 -0400

    finished Read hopefully!

commit 55bfb7675bf1dede5600ccb92bb7ff28b032fc59
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Tue Mar 10 17:32:20 2020 -0400

    worked on read

commit 1c58f2b52b674f9e7d203e3f1f0d420235c27ad3
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Tue Mar 10 17:27:47 2020 -0400

    updating

commit 2227f0f18a44dc7f2375f4092845e7bf1968a779
Author: Peiyuan Xu <pxu11@jhu.edu>
Date:   Tue Mar 10 17:21:17 2020 -0400

    added new files

commit 64360b7812f60bbf8dd1d50b14f0cecbd3ab81ee
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Tue Mar 10 17:05:14 2020 -0400

    added ppm_io.c

commit afe52d2d1c5852d8cfd444441d52b6ce9c3cffcf
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Tue Mar 10 17:03:13 2020 -0400

    started work!

commit 2df0ae868dce61f2d5176656d03c645931894abf
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Tue Mar 10 16:06:12 2020 -0400

    Added all the starter code

commit f45a39740ec9a822bd2dea03693a47ff71d77cef
Author: Lucas Rozendaal <lrozend1@jhu.edu>
Date:   Tue Mar 10 17:19:19 2020 +0000

    README.md created online with Bitbucket
